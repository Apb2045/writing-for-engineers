#!/usr/bin/ruby
$stdout.sync=true;

str='abcdefgh'+'efghefgh';
imax=1024/str.length*1024*4;       # 4mb

starttime=Time.new;
print("exec.tm.sec\tstr.length\n");

gstr='';
i=0;

while i < imax+1000
        i=i+1;
        gstr+=str;
        gstr=gstr.gsub(/efgh/, "____")

        lngth=str.length*i;
        if(lngth % (1024*256)==0)
                print(((Time.new-starttime).ceil).to_s+"sec\t\t",(lngth/1024).to_s,"kb\n");
        end
end

#puts gstr;