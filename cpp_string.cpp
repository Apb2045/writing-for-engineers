
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int main ()
{
  string str = "abcdefgh";
    str += "efghefgh";
  int imax = 1024 /str.length() * 1024 *4;
  time_t currentTime = time(NULL);
  cout << "exec.tm.sec\tstr.length" << endl;

  string find= "efgh";
  string replace ="____";
  string gstr;
  int i=0;
  int length;
//  int end=0; //  size_t end=0;

  while(i++ < imax +1000){
    gstr += str;
    gstr = gstr;
    size_t start, sizeSearch=find.size(), end=0;

    while((start=gstr.find(find,end))!=string::npos){
        end=start+sizeSearch;
        gstr.replace(start,sizeSearch,replace);
    }
    length = str.length()*i;
    if((length%(1024 * 256))==0){
        cout << time(NULL) - currentTime << "sec\t\t" << length/1024 << "kb" <<  endl;
    }
  }
// cout << gstr << endl;

return 0;
}